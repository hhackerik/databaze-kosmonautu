<?php

namespace App\Presenters;

use App\Src\Models\Astronauts\AstronautsFacade;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;


class HomepagePresenter extends Presenter
{
    /**
     * @var AstronautsFacade
     */
    public $astronautsFacade;

    /**
     * @var \IAddAstronautForm
     */
    public $addAstronautForm;

    /**
     * @var null
     */
    public $astronautId = NULL;

    /**
     * HomepagePresenter constructor.
     * @param AstronautsFacade $astronautsFacade
     * @param \IAddAstronautForm $addAstronautForm
     */
    public function __construct(AstronautsFacade $astronautsFacade, \IAddAstronautForm $addAstronautForm)
    {
        $this->astronautsFacade = $astronautsFacade;
        $this->addAstronautForm = $addAstronautForm;
    }

    /**
     * Delete astronaut
     * @param $id
     */
    public function actionDelete($id)
    {
        if($this->astronautsFacade->checkIfAstronautExists($id)) {
            $this->astronautsFacade->deleteAstronaut($id);
            $this->flashMessage("Astronaut byl úspěšně smázán");
        } else {
            $this->flashMessage("Zadaný astronaut neexistuje");;
        }
        $this->redirect("Homepage:default");
    }

    /**
     * Add astronaut
     */
    public function renderAdd()
    {

    }

    /**
     * Edit astronaut
     * @param $id
     */
    public function renderEdit($id)
    {
        if(!$this->astronautsFacade->checkIfAstronautExists($id)) {
            $this->flashMessage("Zadaný astronaut neexistuje");
            $this->redirect("Homepage:default");
        }
        $this->astronautId = $id;
    }

    /**
     * Astronauts list
     */
    public function renderDefault()
    {
        $data = $this->astronautsFacade->getAllAstronauts();
        $this->template->astronauts = $data;
    }

    /**
     *
     * @return \AddAstronautForm
     */
    public function createComponentAddAstronaut()
    {
        $form = $this->addAstronautForm->create($this->astronautId);
        $form['addAstronaut']->onSuccess[] = function (Form $form, $values) {
            if($values->astronautId) {
                $this->flashMessage("Astronaut byl upraven!");
            } else {
                $this->flashMessage("Astronaut byl vytvořen!");
            }

            $this->redirect("Homepage:default");
        };
        return $form;
    }
}