<?php
namespace App\Src\Models;

use Kdyby\Doctrine\EntityManager;
use Nette\Object;

abstract class BaseFacade extends Object
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * BaseFacade constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
}