<?php

namespace App\Src\Models\Astronauts;
use App\Src\Models\BaseFacade;
use Nette\Utils\DateTime;


/**
 * Facade for Astronauts.
 */
class AstronautsFacade extends BaseFacade
{
    /**
     * Get all astronauts
     * @return object
     */
    public function getAllAstronauts()
    {
        return $this->em->getRepository(AstronautsEntity::class)->findBy([],["id" => "DESC"]);
    }

    /**
     * Check if astronaut exists
     * @param $id
     * @return int
     */
    public function checkIfAstronautExists($id)
    {
        return $this->em->getRepository(AstronautsEntity::class)->countBy(array("id" => $id));
    }

    /**
     * Get one astronaut
     * @param $id
     * @return bool|object
     */
    public function getAstronaut($id)
    {
        if(!$id) return FALSE;
        return $this->em->getRepository(AstronautsEntity::class)->findOneBy(array("id" => $id));
    }

    /**
     * Create new astronaut
     * @param $values
     * @return bool
     */
    public function createAstronaut($values)
    {
        $astronaut = new AstronautsEntity();
        $astronaut->name = $values->name;
        $astronaut->surname = $values->surname;
        $astronaut->birthday = new DateTime($values->birthday);
        $astronaut->superPower = $values->superPower;

        $this->em->persist($astronaut);
        $this->em->flush();

        return TRUE;
    }

    /**
     * Edit astronaut
     * @param $id
     * @param $values
     * @return bool
     */
    public function editAstronaut($id,$values)
    {
        $astronaut = $this->getAstronaut($id);
        $astronaut->name = $values->name;
        $astronaut->surname = $values->surname;
        $astronaut->birthday =  new DateTime($values->birthday);
        $astronaut->superPower = $values->superPower;

        $this->em->persist($astronaut);
        $this->em->flush();

        return TRUE;
    }

    /**
     * Delete astronaut
     * @param $id
     * @return bool
     */
    public function deleteAstronaut($id)
    {
        $astronaut = $this->getAstronaut($id);

        if(!$astronaut) return FALSE;

        $this->em->remove($astronaut);
        $this->em->flush();
        return TRUE;
    }
}