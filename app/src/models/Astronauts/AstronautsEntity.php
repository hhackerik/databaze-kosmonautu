<?php
namespace App\Src\Models\Astronauts;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;


/**
 * Doctrine entity for table Astronauts.
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="astronauts")
 */
class AstronautsEntity
{
	use MagicAccessors;
    use Identifier;

	/**
	 * Name column.
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * Surname column.
	 * @ORM\Column(type="string")
	 */
	protected $surname;

	/**
	 * Birthday column.
	 * @ORM\Column(type="datetime")
	 */
	protected $birthday;

    /**
     * Super power column.
     * @ORM\Column(name = "super_power",type="text")
     */
	protected $superPower;

}
