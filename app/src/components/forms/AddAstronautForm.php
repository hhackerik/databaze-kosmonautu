<?php
use \App\Src\Models\Astronauts\AstronautsFacade;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

class AddAstronautForm extends Control
{
    /**
     * @var AstronautsFacade
     */
	public $astronautFacade;

    /**
     * @var null
     */
	public $astronautId;

    /**
     * AddAstronautForm constructor.
     * @param AstronautsFacade $astronautFacade
     * @param null $astronautId
     */
    public function __construct(AstronautsFacade $astronautFacade, $astronautId)
	{
		$this->astronautFacade = $astronautFacade;
		$this->astronautId = $astronautId;
	}

	/**
	 * @return Form
	 */
	public function createComponentAddAstronaut()
	{
		$form = new Form;

        $form->addText('name', 'Jméno')
			->setRequired("Zadejte prosím jméno!");

        $form->addText('surname', 'Přijmení')
			->setRequired("Zadejte prosím přijmení!");

        $form->addText('birthday', 'Datum narození')
			->setRequired("Zadejte prosím datum narození");

        $form->addTextArea('superPower', 'Super schopnost')
            ->setRequired("Zadejte prosím superschopnost");

        $form->addHidden("astronautId", $this->astronautId);

		$form->addSubmit("send", 'Odeslat');

		if($this->astronautId) {
		    $data = $this->astronautFacade->getAstronaut($this->astronautId);
		    $form->setDefaults(['name' => $data->name,
                                'surname' => $data->surname,
                                'birthday' => $data->birthday->format("Y-m-d"),
                                'superPower' => $data->superPower,
                                ]);
        }

		$form->onSuccess[] = [$this, 'astronautProcess'];

		return $form;
	}


    /**
     * @param Form $form
     * @param $values
     */
    public function astronautProcess(Form $form, $values)
	{
	    if($values->astronautId) {
			$this->astronautFacade->editAstronaut($values->astronautId, $values);
		} else {
			$this->astronautFacade->createAstronaut($values);
		}
	}

    /**
     * Form template
     */
    public function render()
	{
		$this->template->render(__DIR__ . '/templates/AddAstronaut.latte');
    }


}

interface IAddAstronautForm
{
	/**
	 *
	 * @return AddAstronautForm
	 */
	function create($astronautId = NULL);
}